package ihor;

import java.lang.String;
import java.util.Random;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        int num;
        int min = 0;
        int max = 100;
        int diff = max - min;
        Random random = new Random();
        int a = random.nextInt(diff + 1);
        a += min;
//        System.out.println("a = " + a );

        System.out.println("Let the game begin!");
        Scanner in = new Scanner(System.in);
        System.out.print("Input your name: ");
        String name = in.next();
        int finalArray[] = new int [1];
        int count = 0;                               //count determines the quantity of tries
        do {

            System.out.print("Input a number: ");
            num = in.nextInt();

            count++;
            int array[];
            if (count == 1){                         //try #1
                array = new int [count];
                array[0] = num;
                finalArray = array;
            } else {                                 //try #2 and more
                array = new int [count];
                for (int j = 0; j < finalArray.length; j++){     //writing down tries minus one
                    array[j] = finalArray[j];
                }
                array[array.length-1] = num;                     //writing down the last try
                finalArray = array;
            }



            if (a < num)
                System.out.println("Your number is too big. Please, try again.");

            else if (a > num)
                System.out.println("Your number is too small. Please, try again.");

            else System.out.printf("Congratulations, %s \n", name + "!");


        } while (a != num);

        //bubble sorting
        int l, m;
        for(l = 0; l < finalArray.length; l++) {
            int Number = finalArray[l];
            for (m = l - 1; m >= 0; m--) {
                int leftNumber = finalArray[m];
                if (Number < leftNumber) {
                    finalArray[l] = leftNumber;
                    finalArray[m] = Number;
                } else {
                    break;
                }
            }
        }
        for (int i: finalArray){
            System.out.printf("Your numbers: %d \n", i);
        }
    }
}
